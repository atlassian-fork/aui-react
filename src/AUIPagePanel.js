import React from 'react';
import PropTypes from 'prop-types';

const AUIPagePanel = props => {
    let nav;
    if (props.nav) {
        nav = (
            <div className="aui-page-panel-nav">
                {props.nav}
            </div>
        );
    }

    let aside;
    if (props.aside) {
        aside = (
            <div className="aui-page-panel-sidebar content-sidebar">
                {props.aside}
            </div>
        );
    }

    return (
        <div className="aui-page-panel" style={props.style}>
            <div className="aui-page-panel-inner">
                {nav}
                <section className="aui-page-panel-content content-body">
                    {props.children}
                </section>
                {aside}
            </div>
        </div>
    );
};

AUIPagePanel.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node)
    ]),
    nav: PropTypes.node,
    aside: PropTypes.node,
    style: PropTypes.object
};

export default AUIPagePanel;

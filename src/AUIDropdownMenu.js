import React from 'react';

// TODO: We need to find a way to explicitly reference the AUI webcomponent rather than implicitly depending on it from the AUI global js.
//if (typeof window !== 'undefined') {
//    require('@atlassian/aui/lib/js/aui/drop-down');
//    require('@atlassian/aui/lib/js/aui/dropdown2');
//}

const AUIDropdownMenu = ({ children, ...otherProps }) => (
    <div {...otherProps} className="aui-dropdown2 aui-style-default">
        {children}
    </div>
);

export default AUIDropdownMenu;

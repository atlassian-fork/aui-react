import React from 'react';
import PropTypes from 'prop-types';

const AUITable = ({ children }) => (
    <table className="aui">
        {children}
    </table>
);

AUITable.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node)
    ])
};

export default AUITable;

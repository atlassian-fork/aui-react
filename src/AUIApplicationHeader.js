import React from 'react';
import PropTypes from 'prop-types';
import AUIApplicationLogo from './AUIApplicationLogo';

const AUIApplicationHeader = ({ logo, headerLink, primaryContent, secondaryContent, text }) => (
    <header id="header" role="banner">
        <nav className="aui-header aui-dropdown2-trigger-group" data-aui-response="true" role="navigation">
            <div className="aui-header-inner">
                <div className="aui-header-primary">
                    <AUIApplicationLogo logo={logo} text={text} headerLink={headerLink ? headerLink : '/'} />
                    {primaryContent}
                </div>
                <div className="aui-header-secondary">
                    {secondaryContent}
                </div>
            </div>
        </nav>
    </header>
);

AUIApplicationHeader.propTypes = {
    logo: PropTypes.oneOf(['aui', 'bamboo', 'bitbucket', 'confluence', 'crowd', 'fecru', 'hipchat', 'jira', 'stash']).isRequired,
    headerLink: PropTypes.string,
    primaryContent: PropTypes.node,
    secondaryContent: PropTypes.string,
    text: PropTypes.string
};

export default AUIApplicationHeader;

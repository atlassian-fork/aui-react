import React from 'react';
import PropTypes from 'prop-types';

const AUINavGroupHorizontal = ({ primary, secondary }) => (
    <nav className="aui-navgroup aui-navgroup-horizontal">
        <div className="aui-navgroup-inner">
            <div className="aui-navgroup-primary">
                {primary}
            </div>
            <div className="aui-navgroup-secondary">
                {secondary}
            </div>
        </div>
    </nav>
);

AUINavGroupHorizontal.propTypes = {
    primary: PropTypes.node,
    secondary: PropTypes.node
};

export default AUINavGroupHorizontal;

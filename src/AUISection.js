import React from 'react';
import PropTypes from 'prop-types';

const AUISection = ({ children, ...otherProps }) => (
    <aui-section {...otherProps}>{children}</aui-section>
);

AUISection.propTypes = {
    children: PropTypes.node
};

export default AUISection;

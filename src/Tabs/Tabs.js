import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Tabs extends PureComponent {
    static getActiveTabFromChildren(children) {
        const tabs = React.Children.toArray(children);
        const activeTabs = tabs.filter(tab => tab.props.isActive);
        const activeTab = activeTabs[0] || tabs[0];

        return activeTab;
    }

    constructor(props) {
        super(props);

        const activeTab = Tabs.getActiveTabFromChildren(this.props.children);

        this.state = {
            activeTabId: activeTab.props.id
        };
    }

    renderTabs() {
        const { activeTabId } = this.state;
        const { children } = this.props;
        const tabsMenu = [];

        const tabsPanes = React.Children.map(children, (child) => {
            const { id, label } = child.props;
            const isActive = activeTabId === id;
            const tabClassName = classnames('menu-item', {
                'active-tab': isActive
            });

            tabsMenu.push(
                <li className={tabClassName} key={id}>
                    <a onClick={() => this.selectTab(id)}>{label}</a>
                </li>
            );

            return React.cloneElement(child, {
                ...child.props,
                key: id,
                isActive
            });
        });

        return { tabsMenu, tabsPanes };
    }

    selectTab(activeTabId) {
        this.setState({ activeTabId });
    }

    render() {
        const { id, className, ...props } = this.props;
        const wrapperClassName = classnames('aui-tabs', 'horizontal-tabs', className);
        const { tabsMenu, tabsPanes } = this.renderTabs();

        delete props.tabPrefix;

        return (
            <div id={id} className={wrapperClassName} {...props}>
                <ul className="tabs-menu">
                    {tabsMenu}
                </ul>

                {tabsPanes}
            </div>
        );
    }
}

Tabs.propTypes = {
    children: PropTypes.node.isRequired,
    id: PropTypes.string,
    className: PropTypes.string
};

Tabs.defaultProps = {
    id: null,
    className: null
};

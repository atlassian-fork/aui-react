const path = require('path');
const { DefinePlugin } = require('webpack');

module.exports = {
    mode: 'production',

    devtool: 'source-map',

    entry: {
        'aui-react': './src/index.js'
    },

    plugins: [
        new DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
    ],

    externals: {
        ajs: 'AJS',
        react: 'React',
        'prop-types': 'PropTypes'
    },

    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].min.js',
        publicPath: '/dist/',
        library: 'AUI',
        libraryTarget: 'umd'
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loaders: 'babel-loader',
                include: path.join(__dirname, 'src')
            }
        ]
    }
};

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import AUIBreadcrumb from '../src/AUIBreadcrumb';

describe('AUIBreadcrumb', () => {
    it('should render the correct AUI markup', () => {
        expect(shallow(<AUIBreadcrumb />).html()).to.equal('<li><a href="#"><span class="aui-nav-item-label"></span></a></li>');
    });
});

import React from 'react';
import { shallow } from 'enzyme';

import { expect } from 'chai';
import AUIPageHeader from '../src/AUIPageHeader';

describe('AUIPageHeader', () => {
    it('should render to correct AUI markup', () => {
        expect(shallow(<AUIPageHeader headerText="Hello world" />).html()).to.equal(`<header class="aui-page-header"><div class="aui-page-header-inner"><div class="aui-page-header-main"><h1>Hello world</h1></div></div></header>`);
    });
});
